import std.stdio;
import std.file;

import bryo;

void main(string[] args)
{
    if (args.length == 2)
    {
        auto prog = cast(string) read(args[1]);
        auto ctx = new EvalContext!void();
        ctx.eval(prog);
    }
}
