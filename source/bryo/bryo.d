module bryo.bryo;

import std.stdio : writeln, write;
import std.conv : text;
import std.uni;
import std.string;
import std.range;
import std.algorithm : chunkBy;
import std.exception : assertThrown;

@safe:

/** Takes a string and returns an input range of tokens. */
struct tokens
{
    string expr;

    /** Constructor */
    this(string initial) pure
    {
        /* skip whitespace, so we are at the start of a token */
        this.expr = initial;
        skipWhite();
    }

    /** Returns if end of range is reached */
    @property bool empty() pure const nothrow @nogc
    {
        return expr.length == 0;
    }

    /** Returns front token */
    @property string front() const
    {
        auto i = endOfTokenIndex(expr);
        return expr[0 .. i];
    }

    /** Advance to next token */
    void popFront()
    {
        auto i = endOfTokenIndex(expr);
        this.expr = expr[i .. $];
        skipWhite();
    }

    private void skipWhite() pure
    {
        bool in_comment = false;
        foreach (i, c; expr.byCodePoint)
        {
            if (c == '#' && !in_comment)
            {
                in_comment = true;
                continue;
            }
            if (c == '\n')
            {
                if (in_comment)
                {
                    in_comment = false;
                    continue;
                }
                else
                {
                    return;
                }
            }
            if (!in_comment && !isWhite(c))
            {
                this.expr = expr[i .. $];
                return;
            }
        }
    }

    private size_t endOfTokenIndex(string s) const
    {
        if (s.empty)
            return 0;
        auto first = s.byCodePoint[0];
        if (first == '\n')
            return 1;
        assert(!isWhite(first));
        if (first == ';')
        {
            return 1;
        }
        else if (first == '{')
        {
            return endOfBracesIndex(s);
        }
        else if (first == '$' && s[1] == '{')
        {
            return endOfBracesIndex(s[1 .. $]) + 1;
        }
        else if (first == '$' && s[1] == '$' && s[2] == '{')
        {
            return endOfBracesIndex(s[2 .. $]) + 2;
        }
        else if (first == '"')
        {
            bool in_quotes = false;
            foreach (i, c; s[1 .. $])
            {
                if (c == '"')
                    return i + 2;
            }
        }
        else
        {
            foreach (i, c; s)
            {
                if (isWhite(c))
                    return i;
                if (c == '{')
                    return i;
                if (c == '"')
                    return i;
                if (c == ';')
                    return i;
                if (c == '}')
                {
                    if (i == 0)
                        throw new SyntaxError("Opening brace missing. Only }, but no {.");
                    return i;
                }
            }
        }
        return s.length;
    }

    private size_t endOfBracesIndex(string s) const
    {
        assert(s[0] == '{');
        size_t depth = 0;
        bool in_quotes = false;
        foreach (i, c; s)
        {
            if (c == '"')
            {
                in_quotes = !in_quotes;
            }
            else if (!in_quotes)
            {
                if (c == '{')
                {
                    depth += 1;
                }
                else if (c == '}')
                {
                    depth -= 1;
                    if (depth == 0)
                        return i + 1;
                }
            }
        }
        throw new SyntaxError("Closing brace missing. Only {, but no }.");
    }
}

/** The only possible syntax error in Bryo
  is a mismatch of braces. */
class SyntaxError : Exception
{
    public this(string msg)
    {
        super(msg);
    }
}

unittest
{
    auto ts = tokens(" foo bar ");
    assert(ts.front == "foo");
    ts.popFront();
    assert(ts.front == "bar");
}

unittest
{
    auto ts = tokens("");
    assert(ts.walkLength == 0);
}

unittest
{
    auto ts = tokens("bla \"foo bar\" blub");
    assert(ts.walkLength == 3);
    ts.popFront();
    assert(ts.front == "\"foo bar\"");
}

unittest
{
    auto ts = tokens("{foo \"{\" bar} blub");
    assert(ts.front == "{foo \"{\" bar}");
    assert(ts.walkLength == 2);
}

unittest
{
    auto ts = tokens("foo{;}bar;bar");
    assert(ts.walkLength == 5);
}

unittest
{
    assertThrown!SyntaxError(tokens("{").array);
    assertThrown!SyntaxError(tokens("}").array);
}

unittest
{
    auto ts = tokens("foo ;; bar");
    assert(ts.walkLength == 4);
}

unittest
{
    auto ts = tokens("bla $bla ${bla bla bla} bla");
    assert(ts.walkLength == 4);
}

unittest
{
    auto ts = tokens("let foo = {\"{bar}\"}; foo");
    assert(ts.walkLength == 6);
}

unittest
{
    auto ts = tokens("if {$current eq \"dev\"} {
            puts \"hello\"
              } else {
            echo \"world\"
        }");
    assert(ts.walkLength == 5);
}

string[] popStatement(ref tokens ts)
{
    string[] statement;
    while (!ts.empty)
    {
        auto t = ts.front;
        ts.popFront();
        if (t == "\n" || t == ";")
        {
            return statement;
        }
        else
        {
            statement ~= t;
        }
    }
    return statement;
}

unittest
{
    auto ts = tokens("foo bar
        baz");
    assert(ts.walkLength == 4);
    auto stmt = ts.popStatement();
    assert(stmt.length == 2);
    assert(ts.walkLength == 1);
}

private bool is_true(string expr) pure nothrow
{
    if (expr.empty)
        return false;
    if (expr == "false")
        return false;
    return true;
}

/** Thrown if command or variable is unknown. */
class EvalException : Exception
{
    public this(string msg)
    {
        super(msg);
    }
}

/**
  Use this to evaluate Bryo programs.

  If the template parameter $(D T) is not void,
  it has a public field $(D T state).
  Use that field for the global state of the programs.
 */
class EvalContext(T = void)
{
    private Namespace ns;
    static if (!is(T == void))
        public T state;

    class Namespace
    {
        string[string] variables;
        string delegate(string[])[string] commands;
        Namespace parent;

        public this(Namespace parent)
        {
            this.parent = parent;
        }

        public string getVar(string name)
        {
            if (name in variables)
                return variables[name];
            if (parent is null)
                throw new EvalException("No variable " ~ name);
            return parent.getVar(name);
        }

        public bool hasVar(string name)
        {
            if (name in variables)
                return true;
            if (parent is null)
                return false;
            return parent.hasVar(name);
        }

        public void setVar(string name, string value)
        {
            variables[name] = value;
        }

        public string delegate(string[]) @safe getCmd(string name)
        {
            if (name in commands)
                return commands[name];
            if (parent is null)
                throw new EvalException("No command " ~ name);
            return parent.getCmd(name);
        }

        public bool hasCmd(string name)
        {
            if (name in commands)
                return true;
            if (parent is null)
                return false;
            return parent.hasCmd(name);
        }

        public void setCmd(string name, string delegate(string[]) @safe cmd)
        {
            commands[name] = cmd;
        }
    }

    /** Instantiates a new evaluation context */
    public this()
    {
        this.ns = new Namespace(null);
        ns.setVar("NL", "\n");
        ns.setVar("Q", "\"");
    }

    /** Evaluates a string as Bry program and returns the result.
       Any variables and commands set within eval, disappear afterwards */
    public string eval(string expr)
    {
        auto old = this.ns;
        /* NOTE This forbids concurrent use! */
        this.ns = new Namespace(old);
        scope (exit)
            this.ns = old;
        return inner_eval(expr, false);
    }

    /** Like eval, but does not enter a new namespace.
      This implies that variables set, will live on. */
    public string eval_no_ns(string expr)
    {
        return inner_eval(expr, false);
    }


    private string inner_eval(string expr, bool recursive)
    {
        auto ts = tokens(expr);
        string ret;
        while (!ts.empty)
        {
            auto stmt = popStatement(ts);
            //writeln("DBG", stmt);
            if (stmt.empty)
                continue;
            /* Evaluate all tokens.
             * We need another array, because the length may change
             * due to double-dollar. */
            string[] args = eval_parameters(stmt);
            ret = "";
            auto first = args[0];
            if (first[0] == '{')
            {
                // TODO what is this for?
                assert(args.length == 1);
                if (recursive)
                    ret = first;
                else
                    ret = inner_eval(first[1 .. $ - 1], true);
            }
            else if (first[0] == '"')
            {
                /* evaluating a string means appending all parameters */
                ret = first[1 .. $ - 1];
                foreach (a; args[1 .. $])
                    ret ~= a;
            }
            else if (first == "set")
            {
                if (args.length > 3)
                    throw new SyntaxError("set has too many arguments");
                if (args[2][0] == '{')
                    ns.setVar(args[1], args[2][1 .. $ - 1]);
                else
                    ns.setVar(args[1], args[2]);
                ret = args[2];
            }
            else if (first == "is_set")
            {
                if (args.length > 2)
                    throw new SyntaxError("is_set has more than 2 arguments: " ~ text(args.length));
                try
                {
                    auto _ = ns.getVar(args[1]);
                    ret = "true";
                }
                catch (EvalException e)
                {
                    ret = "false";
                }
            }
            else
            {
                /* Is the first token a command? */
                if (ns.hasCmd(first)) {
                    auto old = this.ns;
                    /* NOTE This forbids concurrent use! */
                    this.ns = new Namespace(old);
                    scope (exit)
                        this.ns = old;
                    auto cmd = ns.getCmd(first);
                    ret = cmd(args[1 .. $]);
                } else {
                    /* If it is not a command, but a single token, return that */
                    if (args.length == 1)
                        ret = args[0];
                    else
                        throw new EvalException("Cannot eval: " ~ text(args));
                }
            }
        }
        return ret;
    }

    private string[] eval_parameters(string[] stmt) {
        string[] args;
        args.reserve(stmt.length);
        foreach (i, const p; stmt)
        {
            if (p[0] == '$')
            {
                if (p[1] == '$')
                {
                    /* double dollar means to return multiple tokens */
                    if (p[2] == '{')
                    {
                        args ~= tokens(eval(p[3 .. $ - 1])).array;
                    }
                    else
                    {
                        auto name = p[2 .. $];
                        args ~= tokens(ns.getVar(name)).array;
                    }
                }
                else if (p[1] == '{')
                {
                    args ~= eval(p[2 .. $ - 1]);
                }
                else
                {
                    auto name = p[1 .. $];
                    args ~= ns.getVar(name);
                }
            }
            else if (p[0] == '"' && i != 0)
            {
                /* If not the command, strip the quotes.
                   For commands, preserve the quotes for evaluation. */
                args ~= p[1 .. $ - 1];
            }
            else
            {
                args ~= p;
            }
        }
        assert(args.length > 0);
        return args;
    }

    /** Add a command to the current evaluation context.

      You can add commands inside a command (during evaluation),
      but whatever you add disappears, when the command returns.
      */
    void addCommand(string name, string delegate(string[]) @safe cmd)
    {
        ns.setCmd(name, cmd);
    }

    /// ditto
    void addCommand(string name, string function(string[]) @safe cmd) @trusted
    {
        import std.functional : toDelegate;

        ns.setCmd(name, cmd.toDelegate);
    }
}

unittest
{
    auto ctx = new EvalContext!void();
    string cmd(string[] params)
    {
        assert(params[0] == "Hello World!");
        return "";
    }
    ctx.addCommand("echo", &cmd);
    ctx.eval("echo \"Hello World!\"");
}

unittest
{
    auto ctx = new EvalContext!void();
    auto r = ctx.eval("{{foo}}");
    assert(r == "{foo}");
}

unittest
{
    auto ctx = new EvalContext!void();
    auto r = ctx.eval("\"Hello\" $NL World");
    assert(r == "Hello\nWorld");
    r = ctx.eval("\"quote \" $Q \"something\" $Q");
    assert(r == "quote \"something\"");
}


unittest
{
    auto ctx = new EvalContext!void();
    auto r = ctx.eval("set foo bar; $foo");
    assert(r == "bar");
    r = ctx.eval("is_set foo");
    assert(r == "false");
    r = ctx.eval("set foo bar; is_set foo");
    assert(r == "true");
    ctx.eval_no_ns("set foo bar");
    r = ctx.eval("is_set foo");
    assert(r == "true");
}

unittest
{
    auto ctx = new EvalContext!void();
    auto r = ctx.eval("set foo ${bar}; $foo");
    assert(r == "bar");
}

unittest
{
    auto ctx = new EvalContext!void();
    string cmd(string[] params)
    {
        assert(params[0] == "World");
        return "";
    }
    string cmd_if(T)(string[] args)
    {
        auto cond = ctx.eval(args[0]);
        if (is_true(cond))
        {
            return ctx.eval(args[1]);
        }
        else
        {
            assert(args[2] == "else");
            return ctx.eval(args[3]);
        }
    }

    ctx.addCommand("echo", &cmd);
    ctx.addCommand("if", &cmd_if!void);
    auto r = ctx.eval("if \"false\" {echo \"Hello\"} else {echo \"World\"}");
}

/// Adding a custom command to eval context
unittest
{
    struct State
    {
        int x, y;
    }

    auto x = "Hello ";
    auto ctx = new EvalContext!State();
    auto cmd = (string[] args) {
        ctx.state.x += ctx.state.y;
        return x ~ ctx.eval(args[0]) ~ "!";
    };
    ctx.state.x = 42;
    ctx.state.y = 11;
    ctx.addCommand("greet", cmd);
    auto r = ctx.eval("greet \"Max\" \"Payne\"");
    assert(r == "Hello Max!");
    assert(ctx.state.x == 53);
}

/// Double dollar for returning multiple tokens
unittest
{
    auto ctx = new EvalContext!void();
    auto cmd = (string[] args) {
        assert(args.length == 2);
        assert(args[0] == "hello");
        assert(args[1] == "world");
        return "hello world";
    };
    ctx.addCommand("cmd", cmd);
    auto r = ctx.eval("
            set foo {hello world}
            cmd $$foo
            cmd $${cmd hello world}");
    assert(r == "hello world");
}

unittest
{
    auto ctx = new EvalContext!void();
    auto dummy = (string[] args) {
        return "void";
    };
    auto cmd1 = (string[] args) {
        ctx.addCommand("to_disappear", dummy);
        return "void";
    };
    ctx.addCommand("cmd1", cmd1);
    auto cmd2 = (string[] args) {
        assert ("to_disappear" !in ctx.ns.commands);
        return "void";
    };
    ctx.addCommand("cmd2", cmd2);
    auto r = ctx.eval("cmd1; cmd2");
    assert(r == "void");
}
