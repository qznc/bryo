# Bryo

A seed language to build your own language.
Out of the box, Bryo is not a programming language
and certainly not Turing-complete.
You are supposed to
build your own language with it by adding commands.

Bryo is inspired by
[TH1](https://www.fossil-scm.org/xfer/doc/trunk/www/th1.md),
which is a subset of TCL.
Even TH1 comes with a few commands,
while Bryo only comes with the ability to set and get variables.
It can be used if you do not want a "programming" language,
but only a non-turing-complete configuration language.
The syntactical heritage of TCL means
that it looks like commands `foo bar baz`,
not function calls `foo(bar, baz)`.

The name comes from "embryo" and not from "bryology".
Still, if you build something with Bryo,
bryology could provide name inspirations.

My ideas for using Bryo are
a Make-like build tool,
a better cron,
and a continuous integration system.
Bryo shall serve as a more portable shell replacement
and simple configuration language.
You could build a
[sandbox](https://idea.popcount.org/2017-03-28-sandboxing-landscape/)
with it.

## The Language

A *program* is an utf8 text.
*Tokens* are braces, semicolons and everything separated by whitespace.
*Statements* are groups of tokens separated by semicolons and newlines.
The result of the last statement is the result of the whole program.

To evaluate a statement,
first all tokens starting with a dollar `$` are evaluated.
If the dollar is followed by an opening brace `{`,
the inside of the braces is evaluated as a program.
Otherwise, what follows is considered a variable name.
In the resulting token sequence the first token is the *command*;
The rest are *parameters*.
If the command was added to the evaluator,
the corresponding delegate/function is called with the parameters.

There is only two builtin commands:
`set` takes two parameters a and b.
It sets the variable a to the value b.
If the variable did not exist, it is created
in the current and recursive evaluations.
It disappears when the evaluation returns.
Colloquially, variables are dynamically scope with blocks.
The second command is `is_set`,
which takes one parameter
and return "true" if such a variable was set before
or "false" otherwise.

Quoted strings can also be used as commands.
They concatenate all their parameters into one string.
This is necessary, because their is no escaping in strings.
To insert a quotation mark, eval something like
`${"quote " Q "something" Q}`,
which returns `quote "something"`.

The *double-dollar* syntax is used to evaluate to multiple tokens.
It works with variables and statements.
Example:

    set foo {hello world}
    echo $foo    # calls 'echo' with parameters ['hello world']
    echo $$foo   # calls 'echo' with parameters ['hello', 'world']

That is it.
Hopefully complete and concise enough.
